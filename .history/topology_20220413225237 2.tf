resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}







resource "aws_instance" "web" {
  ami           = "ami-0e3f7a235a05f8e99"
  instance_type = "t2.micro"

  tags = {
    Name      = "chalal.abdellatif"
    Formation = "terraform"
    TP        = "TP1"
  }
}
