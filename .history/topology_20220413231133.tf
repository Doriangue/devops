resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.my_vpc.id
}


resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_ig.id
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1a"
  cidr_block        = "10.0.1.0/24"

  tags = {
    Projet = "DevOps"
  }
}

resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}


resource "aws_security_group" "my_security_group" {
  name_prefix = "AKIAQFWWW3BY757JOLEQ"
  vpc_id      = aws_vpc.my_vpc.id

  tags = {
    Projet = "DevOps"
  }
}

resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}


resource "aws_security_group_rule" "my_security_group_rule_out_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}



resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "ìngress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}


# Get latest Ubuntu Linux Bionic Beaver 18.04 AMI
data "aws_ami" "ubuntu-linux-1804" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "my_instance" {
  ami           = data.aws_ami.ubuntu-linux-1804.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.deployer.key_name
}

resource "aws_key_pair" "deployer" {
  key_name   = "ssh_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDw5Pq//pfbHMNs9L1RwUO5OCWZbp1bOgy9pDwD8oQDxvcJg8HGUuQnQh95tL1baQmT1+0z39PG4qiY83rPrC9bjtNAJVmlYPAVzHRSJg+3X3uI1e19fsV5CwpxWwwG3lEuJ31Y8UUb9MLyZP/abIyyLG1nlOWxThGu1am+qmSdSQLZ21b6p+en4N8KMen5YXkPoMdhKeOAI/u8tSjON8XPEN/c6tXXewu7Ep8wCPuMYGmBCOQzRNZpxFDx5KK/DmzgprOsfjViwwiGcsAhdaqbYXK9R1wWJKVBzp8t3tYXUgHcjQxKwfyPbtRWVIGPGM7qrxGr0gAMXZsWL76fm450IyNT/9vzSF3neLDyoTuIsNSMdfSJSxK9te8r1acofXXLfglHv8NXSk9OqRxCUAxy9jjXPUqgXKj9E2+7hvSJNw0eMhoXLVOhorA06qO8Q08R2GyajSLWYF8CPQFGv+dbcrvbjQc7iTGDu9aOWq0+cBWj6x3q4y1mR5dmEdvJeUsX/Ulm8DhLDuOcLLFjuanIKTrajzLmkahbNwF3gmpJt3r6SF5tNW18V6BrUtGQNABJa0VAzAhoqHQPQw27l6FtYJ+WiAJf0CgYLIetRPmOifZia7PXfCei73+YsBR3TJNM68vdzD5G6N4CPUtM4EoQrqq9eg75wNZgXq/o1XsDMw== ubuntu@ip-10-0-1-155"
}

output "ip" {
  value = aws_instance.example.public_ip
}
