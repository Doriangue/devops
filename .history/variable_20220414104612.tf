
variable "aws_project" {
  type        = string
  default     = "gitlab-runner-sr"
  description = "The aws project to deploy the runner into."
}

variable "aws_zone" {
  type        = string
  default     = "eu-west-3"
  description = "The aws region to deploy the runner into."
}

variable "gitlab_runner_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "SSH_KEY" {
  type = string
}
